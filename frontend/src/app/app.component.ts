import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor( private cookieService: CookieService) {}

  download(): void {
    const cookieOptions = {
      expires: 1,
      path: '',
      domain: 'localhost',
      secure: false, // we are not creating https request this time
    };

    // set cookie on window with 'authenticated' value
    this.cookieService.set('auth', 'authenticated', cookieOptions);

    window.open('http://localhost:3000/download', '_blank');
  }
}
