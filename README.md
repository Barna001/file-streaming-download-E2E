# File Streaming Download E2E

How to stream large files effectively from a Node.js backend to a client(Angular) for download.

## Start backend app

- Navigate into backend folder and run `yarn start` after `yarn install` command in a terminal.

## Start client app

- Navigate into frontend folder and run `yarn start` after `yarn install` command in a new terminal.

## Try it out

- Open your favourite browser and type `localhost:4200` into the search bar.
- Click the download button, it will download the example file without extra memory usage.
- You can replace the example file with something really big, rewrite file path in the backend folder to the new name and try again.
