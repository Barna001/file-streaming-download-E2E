import fastify, { FastifyReply, FastifyRequest } from 'fastify';
import cookieParser from 'fastify-cookie';
import cors from 'fastify-cors';
import * as fs from 'fs';
import http from 'http';

class APIError extends Error {
  constructor(public status: number, public message: string) {
    super(message);
  }
}

async function startServer() {

  const fastifyServer = fastify<http.Server>({
    logger: false,
  });

  await fastifyServer.register(cors, {
    origin: "localhost:4200",
  });
  await fastifyServer.register(cookieParser);

  const downloadHandler = async (request: FastifyRequest, response: FastifyReply) => {
    const cookies = request.cookies;
    if (cookies['auth'] !== 'authenticated') {
      throw new APIError(400, 'Unauthorized');
    }
    const readStream = fs.createReadStream('example.png');
    return response.headers({
      'Content-Type': 'image/png',
      'Content-Disposition': 'attachment; filename="example.png"',
    }).send(readStream);
  }

  fastifyServer.get('/download', {}, downloadHandler);
  await fastifyServer.listen(3000);
  console.log('Server started on localhost:3000');
  return fastifyServer;
}

startServer();
